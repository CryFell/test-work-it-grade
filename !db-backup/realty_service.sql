-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 18 2023 г., 07:06
-- Версия сервера: 8.0.30
-- Версия PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `realty_service`
--

-- --------------------------------------------------------

--
-- Структура таблицы `announcement`
--

CREATE TABLE `announcement` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `address` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `price` int NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `isActual` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `announcement`
--

INSERT INTO `announcement` (`id`, `name`, `address`, `description`, `price`, `image`, `isActual`) VALUES
(13, 'Объект 1', 'ул. Мамадышская 45-48', '9-тый дом, 180 кв. метров, двухуровневая', 2000000, 'uploads/167910710416.jpg', 1),
(14, 'Объект 2', 'ул. Гагарина 56-56', 'ул. Азата Аббасова, д. 11, Казань', 3000000, 'uploads/16791072021930134.jpg', 1),
(15, 'Объект 3', 'ул. Ленинградская 97-01', 'частный дом из сруба, 100 кв. метров', 5000000, 'uploads/1679107346dom-gorodishce-141072644-2.jpg', 1),
(16, 'Объект 4', 'ул. Галимджана Баруди 59-78', 'Кирпичный дом, 80 кв. метров', 3500000, 'uploads/1679107441getImage-50.jpeg', 0),
(17, 'Объект 5', 'ул. 50 лет Победы 24-32', 'Панельный дом, 45 кв. метров, с двумя лоджиями', 2800000, 'uploads/16791075251944656.jpg', 0),
(18, 'Объект 6', 'ул. Аббасова 11-22', 'Продается 2-ком кв., 64 м2, 9/19 этаж', 1700000, 'uploads/16791076091930518.jpg', 1),
(19, 'Объект 7', 'ул. Лушникова 50-7', 'Продается 2-ком кв., 45.8 м2, 1/5 этаж', 1800000, 'uploads/1679107679novostroyka-usady-197995467-2.jpg', 0),
(20, 'Объект 8', 'ул. Широка 97-01', 'Продается 2-ком кв., 63 м2, 9/10 этаж', 4200000, 'uploads/1679107753Объект 1.jpg', 0),
(21, 'Объект 9', 'ул. Хо Ши Мина 56-321', 'Продается 4-ком кв., 83.5 м2, 10/11 этаж', 10000000, 'uploads/1679107826самая-дорогая-квартира-в-Москве.jpg', 0),
(22, 'Объект 10', 'ул. Фучика 6-97', 'Продается 2-ком кв., 62.8 м2, 2/16 этаж', 3600000, 'uploads/1679107911dom-niva-178145527-2.jpg', 0),
(23, '123123', '123123', '123123', 123123, 'uploads/1679111643настройки.png', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(2, 'admin', 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
