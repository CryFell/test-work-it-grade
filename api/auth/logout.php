<?php

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

Header('Access-Control-Allow-Origin: *');
Header('Content-Type: application/json');
Header('Access-Control-Allow-Method: GET');

if($_SESSION['user']){
    unset($_SESSION['user']);
    echo json_encode(['status' => 'ok', 'message' => 'user logout']);
} else {
    echo json_encode(['status' => 'err', 'message' => 'user not login']);
}
?>
