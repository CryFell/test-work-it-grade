<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

Header('Access-Control-Allow-Origin: *');
Header('Content-Type: application/json');
Header('Access-Control-Allow-Method: GET');

include_once('../../config/database.php');
include_once('../../models/user.php');

$database = new Database;
$db = $database->connect();

$user = new User($db);

$login = $_POST['login'];
$password = $_POST['password'];

$data = $user->login($login, $password);

if($data->rowCount()){
    $account = array();

    while($row = $data->fetch(PDO::FETCH_OBJ)){
        $account = [
            'login' => $row->login,
        ];
    }

    $_SESSION['user'] = $account['login'];

    echo json_encode($account);

} else {
    echo json_encode(['status' => 'err', 'message' => 'account not found']);
}


?>
