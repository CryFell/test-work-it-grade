<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

Header('Access-Control-Allow-Origin: *');
Header('Content-Type: application/json');
Header('Access-Control-Allow-Method: GET');

include_once('../../config/Database.php');
include_once('../../models/announcement.php');

$database = new Database;
$db = $database->connect();

$announcement = new Announcement($db);

$data = $announcement->readAnnouncements(isset($_SESSION['user']), $_GET['sort']);

if ($data->rowCount())
{
    $announcements = [];

    while($row = $data->fetch(PDO::FETCH_OBJ))
    {
        $announcements[] = [
            'id' => $row->id,
            'name' => $row->name,
            'address' => $row->address,
            'description' => $row->description,
            'price' => $row->price,
            'isActual' => $row->isActual,
            'image' => $row->image,
        ];
    }

    echo json_encode($announcements);
}
else
{
    echo json_encode(['message' => 'No announcement found']);
}
