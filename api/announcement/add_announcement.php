<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

Header('Access-Control-Allow-Origin: *');
Header('Content-Type: application/json');
Header('Access-Control-Allow-Method: GET');

include_once('../../config/database.php');
include_once('../../models/announcement.php');

$database = new Database;
$db = $database->connect();

$announcement = new Announcement($db);

$path = 'uploads/'.time().$_FILES['image']['name'];
move_uploaded_file($_FILES['image']['tmp_name'], '../../'.$path);

$announcement = $announcement->add_announcement($_POST['name'], $_POST['address'], $_POST['description'], $_POST['price'], $path, $_POST['isActual']);

if($announcement->rowCount()){
    echo json_encode(['status' => 'ok', 'message' => 'post added', 'path' => $path]);
} else {
    echo json_encode(['status' => 'err', 'message' => 'post no added']);
}
?>