<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

Header('Access-Control-Allow-Origin: *');
Header('Content-Type: application/json');
Header('Access-Control-Allow-Method: GET');

include_once('../../config/database.php');
include_once('../../models/announcement.php');

$database = new Database;
$db = $database->connect();

$announcement = new Announcement($db);

if(isset($_GET['id']))
{
    $announcement = $announcement->readAnnouncement($_GET['id']);

    if($announcement->rowCount())
    {
        $announcements = [];

        while($row = $announcement->fetch(PDO::FETCH_OBJ))
        {
            $announcements[] = [
                'id' => $row->id,
                'name' => $row->name,
                'address' => $row->address,
                'description' => $row->description,
                'price' => $row->price,
                'isActual' => $row->isActual,
                'image' => $row->image,
            ];
        }
        echo json_encode($announcements);
    }
    else
    {
        echo json_encode(['message'=> 'No data']);
    }
}