<?php
session_start();

?>
<!doctype html >

<html>

<head>
    <meta charset="utf-8">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>


    <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

</head>

<body>
<div class="container">
    <header>
        <h1 class="text-center"> Тестовое задание </h1>
    </header>

    <div class="row">
        <div class="col-8">
            <div>
                <select class="sort form-select">
                    <option value="ASC" selected="selected">По возрастанию</option>
                    <option value="DESC">По убыванию</option>
                </select>
            </div>
            <div class="cards"></div>
            <div class="add_cards col-8">
                <form>
                    <div class="row g-3">
                        <div class="col">
                            <input type="text" class="card-name form-control" placeholder="Имя" aria-label="Имя">
                        </div>
                        <div class="col">
                            <input type="text" class="card-address form-control" placeholder="Адресс" aria-label="Адресс">
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col">
                            <textarea class="card-description form-control" id="exampleFormControlTextarea1" placeholder="Описание" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-2">
                            <input type="text" class="card-price form-control" placeholder="Цена" aria-label="Цена">
                        </div>
                        <div class="col-4">
                            <select class="card-isActual form-select" aria-label="Пример выбора по умолчанию" >
                                <option value="1" selected="selected">Актуально</option>
                                <option value="0">Не актуально</option>
                            </select>
                        </div>
                        <div class="col">
                            <input type="file" class="card-image form-control" id="customFile" />
                        </div>
                        <div class="col">
                            <button type="button" class="send-btn btn btn-primary">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-4">
            <div class="user">
                <div class="logout">
                    <form>
                        <div class="mb-3">
                            <p class="user-name">Вы авторизованы как: <?php echo $_SESSION['user'] ?> </p>
                        </div>
                        <button type="button" class="logout-btn btn btn-primary">Выйти</button>
                    </form>
                </div>

                <div class="login">
                    <form>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label"> Логин </label>
                            <input type="text" class="user-login form-control" id="exampleFormControlInput1"
                                   placeholder="Введите логин">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput2" class="form-label"> Пароль </label>
                            <input type="text" class="user-password form-control" id="exampleFormControlInput2"
                                   placeholder="Введите пароль">
                        </div>
                        <button type="button" class="login-btn btn btn-primary">Войти</button>
                    </form>


                    <?php if ($_SESSION['user']) {
                        ?>
                        <script>
                            $('.login').hide();
                        </script>
                    <?php } else { ?>
                        <script>
                            $('.logout').hide();
                            $('.add_cards').hide();
                        </script>
                    <?php } ?>

                </div>

            </div>
        </div>
    </div>


</div>
</div >
<script src="assets/js/main.js"></script>
</body>