<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

class Announcement
{
    public $id;
    public $name;
    public $address;
    public $description;
    public $price;
    public $isActual;

    private $connection;
    private $table = 'announcement';

    public function __construct($db)
    {
        $this->connection = $db;
    }

    public function readAnnouncements($isAuth, $sort)
    {
        if($isAuth){
            $query = "SELECT * FROM $this->table ORDER BY price $sort";
        } else {
            $query = "SELECT * FROM $this->table WHERE isActual = 1 ORDER BY price $sort";
        }

        $post = $this->connection->prepare($query);

        $post->execute();

        return $post;
    }

    public function readAnnouncement($id)
    {
        $this->id = $id;

//        $query = 'SELECT * FROM '.$this->table.'WHERE id=:id';
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id=?';

        $post = $this->connection->prepare($query);

        $post->bindValue(1, $this->id, PDO::PARAM_INT);
        $post->execute();

//        $post->execute([$this->id]);

        return $post;

    }

    public function add_announcement($name, $address, $description, $price, $image, $isActual)
    {
        $query = "INSERT INTO $this->table (`name`, `address`, `description`, `price`, `image`, `isActual`) VALUES ('$name', '$address', '$description', '$price', '$image', '$isActual') ";
        $post = $this->connection->prepare($query);
        $post->execute();

        return $post;
    }
}