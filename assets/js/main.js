function update_announcement() {
    $('.cards').empty()
    $.ajax(
        'api/announcement/announcements.php'+"?sort="+sort,
        {
            success: function (data) {
                data.map((el, index) => {
                    console.log(el);
                    $('.cards').append(`
                                    <div class="card mb-3">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                                <img src="${el.image}" class="img-fluid rounded-start" alt="..." />
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h5 class="card-title">Имя: ${el.name}</h5>
                                                    <h5 class="card-title">Адресс: ${el.address}</h5>
                                                    <p class="card-text">Описание: ${el.description}</p>
                                                    <p class="card-text">Цена: ${el.price}</p>
                                                    <p class="card-text">Актуальность: ${el.isActual}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                `)
                })
            },
            error: function () {
                alert('There was some error performing the AJAX call!');
            }
        }
    );
}


$('.login-btn').click((e) => {
    e.preventDefault()

    let login = $('.user-login').val(),
        password = $('.user-password').val()

    $.ajax({
        url: 'api/auth/login.php',
        method: 'POST',
        dataType: 'json',
        data: {
            login: login,
            password: password
        },
        success: function (data) {
            // console.log(data)
            $('.login').hide();
            $('.logout').show();
            $('.user-name').empty()
            $('.user-name').append(`Вы авторизованы как: ${data.login}`)
            $('.add_cards').show();

            update_announcement()
        }
    })
})

$('.logout-btn').click((e) => {
    e.preventDefault()
    $.ajax(
        'api/auth/logout.php',
        {
            success: function (data) {
                if (data['status'] === 'ok') {
                    $('.login').show();
                    $('.logout').hide();
                    $('.add_cards').hide();

                    update_announcement()
                }
            }
        }
    )
})

let image = false

$('.card-image').change((e) => {
    image = e.target.files[0];
})


$('.send-btn').click((e) => {
    e.preventDefault();

    let name = $('.card-name').val(),
        address = $('.card-address').val(),
        description = $('.card-description').val(),
        price = $('.card-price').val(),
        isActual = $('.card-isActual').val()

    let formData = new FormData();
    formData.append('name', name);
    formData.append('address', address);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('isActual', isActual);
    formData.append('image', image);

    $.ajax({
        url: 'api/announcement/add_announcement.php',
        method: 'POST',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (data) {
            update_announcement()
        }
    })
})

let sort = $('.sort').val()

$('.sort').change((e) => {
    sort = e.target.value;
    update_announcement()
})

$(document).ready(()=>{


    update_announcement()
})